#!/bin/sh

# ---------------------------------------------------------
# Strikr Free Software Community Project
# https://strikr.io/
# ---------------------------------------------------------


CONF_IP_ADDRESS=127.0.0.1
CONF_IP_PORT=8888

mkdocs serve                        \
       --strict                     \
       --use-directory-urls         \
       --config-file ../mkdocs.yml  \
       --verbose                    \
       --livereload                 \
       --dev-addr ${CONF_IP_ADDRESS}:${CONF_IP_PORT}


exit 0;


# STRIKR commentary
# 2021-07-18 updated
