
Whether we organize a in-person or digital event, you can be assured of a safe environment for all the attentees.


Please note the following

- Event start [timings](../timings.md) (UTC+0000) 0800 hrs.
- Event type could be a [Saturday Meet](../meet.md) (90 min) or a [Hackathon](../hackathon.md) (240 min).
- We use [JITSI](../jitsi.md) to participate and interact.
- Please introduce yourself, when you join the meet.
- [Code of Conduct](../../governance/conduct.md) applies. violations get reported.
- Anonymous profiles get removed without notice.
- All are welcome. Right to admission is reserved.


##### Event Schedule

| Date                         | Type       | Topic, Focus Area                                                |
|:-----------------------------|:-----------|:-----------------------------------------------------------------|
| [2021-07-17](2021-07-17.md)  | Sat Meet   | Community Kickstart Meet                                         |
| [2021-07-24](2021-07-24.md)  | Sat Meet   | Choice of Programming Languages                                  |
| [2021-07-25](2021-07-25.md)  | WADR       | Issue with Issue Tracking systems                                |
| [2021-08-07](2021-08-07.md)  | Sat Meet   | Own the shell with PERL (sh,sed,awk,regex,expect,tcl are us)     |
| [2021-08-08](2021-08-08.md)  | Hackathon  | PERL Applied Hackathon                                           |
<!--
| [2021-08-21](2021-08-21.md)  | Sat Meet   | Getting started with Emacs for Fun and Productivity              |
| [2021-09-04](2021-09-04.md)  | Sat Meet   | The Joy of programming with Modern C++20                         |
| [2021-09-18](2021-09-18.md)  | Sat Meet   | Systems Programming using Modern C++20                           |
| [2021-10-02](2021-10-02.md)  | Sat Meet   | Project Kickoff Meet                                             |
| [2021-10-16](2021-10-16.md)  | Sat Meet   | Cloud Computing Platforms                                        |
| [2021-11-06](2021-11-06.md)  | Sat Meet   | Concurrent Programming with Modern C++20                         |
| [2021-11-20](2021-11-20.md)  | Sat Meet   | Linux Namespace and Control Groups                               |
| [2021-12-04](2021-12-04.md)  | Sat Meet   | Orchestration Platforms                                          |
| [2021-12-18](2021-12-18.md)  | Sat Meet   | Programming Network Servers with Modern C++20                    |
-->