

![Strikr Mailing list](../img/tux_email.png)


##### Why E-mail

Email based communication using a mailing list among free software community members provides unparalleled advantages. It is the most efficient way to share thoughts without any limitation at any time from any location at your own convenience.

* async (model)
* content length not limited
* broadcast oriented
* archivable
* searchable


##### Mailing List

* Subscribe
  send a blank email to [foss-subscribe@strikr.io](mailto:foss-subscribe@strikr.io)
* UNsubscribe
  send a blank email to [foss-unsubscribe@strikr.io](mailto:foss-unsubscribe@strikr.io)
* Post
  to post, write an email to [foss@strikr.io](mailto:foss@strikr.io)


##### List Archives

Mailing list archives are available at [https://www.mail-archive.com/foss@strikr.io/](https://www.mail-archive.com/foss@strikr.io/)


<hr>

image credit: need help to locate the author and attribute the copyright. if you know, please write to legal@strikr.io