
![Strikr Hackathon](../img/tux_hackathon.png)

##### Community Hackathon


##### Structure

- Start with a problem statement or well defined goal
- Multiple sprints scheduled if the work break-down structure so requires
- Event [scheduled](event/index.md) during day in a 4-hour time boxed format
- [timings](timings.md) (UTC+0000) 0800 - 1200 hrs
- Co-ordinated over [JITSI](jitsi.md) and [IRC](irc.md)
- [Code of conduct](../governance/conduct.md) applies. no exceptions. violators get removed.
- Git repo for hackathon maintained under [Strikr organization](https://codeberg.org/strikr) on [Codeberg](https://codeberg.org/).
- All source released under [AGPLv3+](../governance/license/agplv3.md) license and all documentation, visual graphics under [GFDLv3+](../governance/license/gfdlv3.md) license.


##### Upcoming Hackathons

Please checkout the [Events Schedule](event/index.md).


##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />

art credit: pngwing.com, free-png-niapn