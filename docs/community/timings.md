
![Strikr Timings](../img/tux_office.png)


All Strikr community interactions start at a fixed well-defined global time.

- [daily community hours](daily.md)
- [saturday meets](meet.md)
- [hackathons](hackathon.md)


##### Global

- start time *(UTC+0000)* **0800** hrs


##### City specific timings

As a courtesy we provide local timings for some cities.

- (UTC-03:00) 05:00 São Paulo
- (UTC+00:00) 08:00 ---- Event Baseline ----
- (UTC+00:00) 08:00 London (DST may apply)
- (UTC+01:00) 09:00 Algiers
- (UTC+01:00) 09:00 Berlin
- (UTC+01:00) 09:00 Rabat
- (UTC+01:00) 09:00 Stockholm
- (UTC+01:00) 09:00 Tunis
- (UTC+02:00) 10:00 Cairo
- (UTC+02:00) 10:00 Damascus
- (UTC+02:00) 10:00 Khartoum
- (UTC+03:00) 11:00 Ankara
- (UTC+04:00) 12:00 Dubai
- (UTC+05:00) 13:00 Islamabad
- (UTC+05:30) 13:30 New Delhi
- (UTC+06:00) 14:00 Dhaka
- (UTC+07:00) 15:00 Hanoi
- (UTC+07:00) 15:00 Jakarta
- (UTC+08:00) 16:00 Beijing
- (UTC+08:00) 16:00 Kuala Lampur
- (UTC+08:00) 16:00 Singapore
- (UTC+09:00) 17:00 Tokyo
- (UTC+09:00) 19:00 Sydney
- (UTC+13:00) 21:00 Auckland



##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.
