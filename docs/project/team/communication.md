
![Strikr Mailing list](../../img/tux_email.png)

We need to be able to collaborate effectively if we are to realize our vision of removing inequality by production of knowledge.

Effective communication between all team members helps develop a sense of belonging, we watch each others back and build a momentum towards what we set out to achieve.

We use a combination of **async** and **real-time** interaction to span multiple timezones and priorities.


##### We use IRC & JITSI 

We make good use of real-time **sync** up mode

- for all project meetings
- to discuss an important issue among ourselves ([JITSI](../../community/jitsi.md) for audio, video and screen sharing).
- to jointly troubleshoot or debug an issue (**\#strikr** [IRC](../../community/irc.md) channel).


##### We use E-mail

Email based communication using a mailing list among free software community members provides unparalleled advantages.

It is the most efficient way to share thoughts without any limitation at any time from any location at your own convenience.

- **async** (mode)
- content length not limited
- broadcast oriented
- add tags in subject line
- filter on tags, strings
- archivable
- searchable


##### NOTE

We use **one mailing list** for ALL projects.

This may seem unusual, however, we value *ambient awareness* in community powered software engineering.

Given our unique approach to engineering software, all project members are updated with the development happening in different projects. Everyone is welcome to chime-in, share their perspective and also negotiate changes that may be required for a particular functionality to be effectively realized. With smart usage of [tags] in subject line, it is easy to filter content.

Folks who are curious or are interfacing multiple modules will have all the updates and can **free**ly participate in any of the discussions.

We help each other succeed.


#####  Mailing list

- to post, write to [free@strikr.io](mailto:free@strikr.io)
- archives are open and available at [https://www.mail-archive.com/free@strikr.io/](https://www.mail-archive.com/free@strikr.io/)
- posting is subscriber-only
- subscription is invite-only
