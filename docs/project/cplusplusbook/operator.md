
![Strikr C++ book](../../img/cc_book.png)

##### ISO C++ Operator Precendence Table

The operators are listed in **decreasing** order of priority.


| Operator                    | Description                 | Order of evaluation                                            |
:-----------------------------|:----------------------------|:---------------------------------------------------------------|
|```.  ->  []```              |member element access        |**(since C++17)** eval args ```L -> R```                        |
|```       ()```              |function call                |**(since C++17)** func_expr before args                         |
|```++, --```                 |increment, decrement         |++x differs from x++                                            |
| ```-, !, ~```               |negate, not, bitwise not     |+x also supported                                               |
| ```.*, ->*```               |pointer member access        |**(since C++17)** eval args ```L -> R```                        |
| ```*, /, %```               |multiply, divide, modulo     |with integral operands, the result is integral                  |
| ```+, -```                  |add, subtract                |-                                                               |
| ```<<, >>```                |shift and I/O                |**(since C++17)** eval args ```L -> R```                        |
| ```<, <=, >=, >```          |less and greater (or equal)  |-                                                               |
| ```==, !=```                |equal, not equal             |-                                                               |
| ```&```                     |bitwise AND                  |-                                                               |
| ```^```                     |bitwise XOR                  |-                                                               |
| ```|```                     |bitwise OR                   |-                                                               |
| ```&&```                    |logical AND                  |eval args L -> R until first false                              |
| ```||```                    |logical OR                   |eval args L -> R until first true                               |
| ```=, +=, -=, *=, ...```    |Assign                       |**(since C++17)** x += a is x = x + a, eval args ```L <-- R```  |
| ```?:```                    |Conditional evaluation       |-                                                               |
| ```,```                     |Sequence of expressions      |eval args L -> R                                                |


---
Note:

* important changes and clarifications since **C++17**.