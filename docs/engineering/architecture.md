
Resilience, Recoverability, Reconfigurability, Recombinability are highly valuabled attributes of infrastructure software.


##### Software Architecture

Software is defined on Algebraic Structures.

Large structures are built by combining smaller structures which are built by combining even smaller structures.


##### Overview

We highly recommend, Sean Parent's talk 'Better Code: Relationships' presented at ACCU 2021.

<iframe width="560" height="315" src="https://www.youtube.com/embed/f5UsHQW7-9w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### Reference

- Alexander Stepanov, From Mathematics to Generic Programming.
- [Alexander Stepanov](http://stepanovpapers.com/)

