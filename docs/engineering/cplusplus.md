
![ISO C++](../img/cc.png)

##### Modern C++20/23

C++ is a industrial programming language and the invisible foundation of modern software infrastructure.


##### Bjarne Stroustrup

Prof. Bjarne Stroustrup is the original creator of C++.

Bjarne has rich and varied experience that stems from his knowledge of multiple programming languages, his decades of experience in software engineering, his background in teaching at universities TAMU, Columbia, his professional work at Morgan Stanley and his passion and dedication to making sure that C++ is the best possible language it can be for the next generation of programmers.

We encourage you to watch the recent (2021-02-24) interview of Dr. Bjarne Stroustrup.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8aIqIBmNJyM?start=283" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hope you enjoyed the video and also learnt what makes C++ such an amazing programming language.


##### C++ the next 40 years

With a release cadence every 3 years, C++ is now a language  where new features, capabilities, bug fixes and deprecations are happening very actively thanks to a proactive community and a reinvigorated standards committee.

The WG21 aka [C++ committee](https://isocpp.org/std/the-committee) has grown from strength to strength over the years. There are

![ISO C++ WG21](../img/cc_wg21.png)

When we talk about evolution, we really mean it across three critical dimensions of resurgent growth

* the ```core C++``` language
* the ```C++ guidelines``` for engineers
* the ```C++ Standard library``` which provides containers, algorithms, data structures.

Rest assured, C++ is the language you can look forward to for the next 40 years.


##### Toolchain

![GNU GCC](../img/cc_gnu_gcc.png)

GCC aka [GNU Compiler Collection](https://gcc.gnu.org/) is a world-class free software compiler toolchain for C++.

libstdc++, is the [GNU C++ standard library](https://ref.strikr.io/) which is a collection of functions, classes that provide range of functionality.

GDB aka [GNU debugger](https://www.gnu.org/software/gdb/) is a feature rich debugging environment for modern C++ programs, libraries and frameworks.

All Strikr projects are baselined on ```GCC - libstdc++ - GDB - Linux - GLIBC``` combination.


##### Sample


Here is a sample code

```cpp
#include <iostream>

auto main () -> int {

     std::string sfsc {"Strikr Free Software Project"};

     std::cout << sfsc << '\n';


return 0;
}
```

and to compile this code on a GNU/Linux system like [ArchLinux](https://archlinux.org)

```bash
g++ -std=c++20 -g sfsc.cc
```


##### C++ Core Guidelines


Benefits

* Complete type and resource safety
* No memory corruption
* No leaks
* No garbage collection
* No runtime overhead
* ISO C++ language
* Simpler Code
* Tool enforced

Learn more by watching Dr. Bjarne Stroustrup talk.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BZTSs_d6kYI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




Welcome to the amazing world of programming in modern C++20/23 on GNU/Linux system.


---
image credit: isocpp.org website