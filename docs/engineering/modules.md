
![C++ module](../img/cc_modules.png)

#### C++20 Modules

In C++, the embodiment of a *source unit* is the *translation unit*. In simple words, we can call it the C++ source file.

Now, with the introduction of modules, we have a new type of translation unit called the *module unit*.

The impact is immediate !

When we write *legacy-free* modern C++20 code, we can now choose to write a module instead of a header file. We can now create a type-safe binary artifact instead of relying on macro hackery, C pre-processor and header file.

```
import strikr.json;
```

instead of

```
#include "strikr/json.h"
```

There is much more to modules and a lot of innovation is possible. As we evolve this article, stay tuned for the details.


##### C++20 Modules presentation

Florian et' al presentation on [C++20 Modules](https://www.linuxplumbersconf.org/event/7/contributions/751/) at GNU Tools Track (day 2), Linux Plumbers Conference 2020.

<iframe width="560" height="315" src="https://www.youtube.com/embed/oMOH79wpqOw?start=3392" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



##### Reference

- [C++ modules paper P1103r3](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1103r3.pdf)
- [P1103r3 merge to C++ draft](https://github.com/cplusplus/draft/issues/2691)
- [C++20 Modules, a brief tour](https://accu.org/journals/overload/28/159/sidwell/)
- [C++20 Modules, LPC 2020](https://www.linuxplumbersconf.org/event/7/contributions/751/attachments/502/929/linux-plumbers.pdf)
- [GCC C++20 Modules Wiki](https://gcc.gnu.org/wiki/cxx-modules)


<hr />
image credit: FSF, GNU, GCC project.