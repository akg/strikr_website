
![GNU GCC Library](../img/gnu_gcc.png)

##### C++ Standard Library Documentation

the C++ Standard Library is a collection of classes and functions, which are written in the core language and part of the C++ ISO Standard itself. A unique feature of the C++ standard library is it not only specifies the syntax and semantics of generic algorithms, but also places requirements on their performance.


##### Overview

GNU C++ Library aka **libstdc++** is a free software implementation of the ISO C++ standard and is part of the GCC source code made available under the terms of the GPLv3 license.


##### C++ Library Components

- Algorithms
- Atomics
- Concurrency
- Containers
- Diagnostics
- Exceptions
- Extensions
- File System
- I/O
- Iterators
- Locales
- Numerics
- Random Number Generation
- Regular Expressions
- Strings
- Technical Specifications
- Utilities

Take a look at the [C++ library components](https://ref.strikr.io/libstdcxx/api/modules.html).


##### C++ std:: Namespace

The toplevel namespace is **[std](https://ref.strikr.io/libstdcxx/api/a01774.html)**.

**std** contains the following namespaces in addition to many classes.

- __cxx11
- __debug
- __detail
- __exception_ptr
- __parallel
- __parse_int
- __select_int
- _V2
- chrono
- decimal
- execution
- experimental
- filesystem
- literals
- placeholders
- pmr
- regex_constants
- rel_ops
- this_thread
- tr1
- tr2

Take a look at the complete list of [all C++ namespaces](https://ref.strikr.io/libstdcxx/api/namespaces.html).


##### C++ Classes

Some commonly access C++ classes are

Data Structure

- [array](https://ref.strikr.io/libstdcxx/api/a01975.html)
- [slice](https://ref.strikr.io/libstdcxx/api/a07051.html)
- [pair](https://ref.strikr.io/libstdcxx/api/a07779.html)
- [tuple](https://ref.strikr.io/libstdcxx/api/a03231.html)
- [vector](https://ref.strikr.io/libstdcxx/api/a18032.html)
- [set](https://ref.strikr.io/libstdcxx/api/a18012.html)
- [list](https://ref.strikr.io/libstdcxx/api/a17996.html)
- [map](https://ref.strikr.io/libstdcxx/api/a18000.html)
- [bitset](https://ref.strikr.io/libstdcxx/api/a17984.html)
- [hash](https://ref.strikr.io/libstdcxx/api/a03159.html)
- [stack](https://ref.strikr.io/libstdcxx/api/a07807.html)

Pointer

- [shared_ptr](https://ref.strikr.io/libstdcxx/api/a06827.html)
- [unique_ptr](https://ref.strikr.io/libstdcxx/api/a07927.html)

Type

- [any](https://ref.strikr.io/libstdcxx/api/a01935.html)
- [optional](https://ref.strikr.io/libstdcxx/api/a02939.html)
- [variant](https://ref.strikr.io/libstdcxx/api/a04303.html)

Concurrency

- [thread](https://ref.strikr.io/libstdcxx/api/a07147.html)
- [atomic](https://ref.strikr.io/libstdcxx/api/a01999.html)
- [mutex](https://ref.strikr.io/libstdcxx/api/a07123.html)
- [future](https://ref.strikr.io/libstdcxx/api/a02527.html)
- [promise](https://ref.strikr.io/libstdcxx/api/a02539.html)

Functional

- [enable_if](https://ref.strikr.io/libstdcxx/api/a04079.html)
- [equal_to](https://ref.strikr.io/libstdcxx/api/a07375.html)
- [function](https://ref.strikr.io/libstdcxx/api/a07071.html)


Take a look at the [complete list of class in the std namespace](https://ref.strikr.io/libstdcxx/api/a01774.html)

We'd suggest that in the interest of time, please use the search box on the top right.

Take a look at the comprehensive [list of classes](https://ref.strikr.io/libstdcxx/api/annotated.html).


##### C++ Class Hierarchy

Take a look at the textual [C++ class hierarchy](https://ref.strikr.io/libstdcxx/api/hierarchy.html)

Take a look at the *visual* UML based representation of the [C++ class hierarchy](https://ref.strikr.io/libstdcxx/api/inherits.html)


##### C++ STL Requirements Table

As part of the ISO standardization of the C++ library, the requirements on containers are presented in a [tabular format](https://ref.strikr.io/libstdcxx/api/tables.html).

As you will note, this helps see the container design in its most generic form.


##### Reference

- C++ [library reference](https://ref.strikr.io/)
- C++ [language reference](https://en.cppreference.com/w/)


<hr />
image credit: Free Software Foundation, GNU GCC project.