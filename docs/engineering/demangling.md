
#### C++ Demangling

Understanding the sequence of calls in binutils for implementing C++ demangling.


```uml-sequence-diagram
Title: C++ demangling call flow
main            -> "cxxfilt.c"     : demangle_it()
"cxxfilt.c"     -> "cplus-dem.c"   : cplus_demangle()
"cplus-dem.c"   -> "cp-demangle.c" : cplus_demangle_v3()
"cp-demangle.c" -> "cp-demangle.c" : d_demangle()
"cp-demangle.c" -> "cp-demangle.c" : d_demangle_callback()
```
