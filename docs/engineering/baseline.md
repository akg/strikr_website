
Consistent with our venerable [goals](../governance/goals.md) and our cherished [values](../governance/values.md), we have baselined the core building blocks of our project.


![Strikr triplet GNULinux - Cplusplus - AGPLv3](../img/triplet.png)


##### Baseline

| Artifact              | Choice                    | C-orig | Rationale                                                          |
| ----------------------| --------------------------| -------| -------------------------------------------------------------------|
| Language              | C++                       | no bar | C++20/23; paradigm agnostic; type safe; very strong innovator community       |
| Language              | PERL                      | no bar | v5.8.0+; runs everywhere; own the shell; regex power               |
| Language              | GNU Emacs                 | no bar | GPL; free software; also an editor; ELisp                          |
| Kernel                | Linux                     | no bar | GPL; free software                                                 |
| Userland              | GNU glibc                 | no bar | GPL; free software                                                 |
| Utilities             | GNU tools                 | no bar | GPL; free software                                                 |
| Toolchain             | GNU gcc, g++              | no bar | GPL; free software                                                 |
| Version control       | Git                       | no bar | GPL; free software                                                 |
| Virtualization        | KVM                       | no bar | GPL; free software                                                 |
| Emulation             | QEMU                      | no bar | GPL; free software                                                 |
| security              | GNU TLS                   | no bar | LGPLv2.1+; free software; EU                                       |
| security              | OpenSSL                   | no bar | ASL 2.0 since v3.x; weak copyleft; Canada                          |
| security              | OpenSSH                   | no bar | BSD; Canada                                                        |
| Containerization      | Open Container Initiative | no bar | runtime-spec, image-spec                                           |
| Other Ecosystems      | Interface                 | no bar | target the interface; royalty-free spec; reverse engineer for interoperability  |
| Software License      | intent based license      | no bar | AGPLv3+ (source); GFDLv3+ (docs); CC-BY-NC-ND 4.0 (commercial)     |


##### Barred

using any MAFANGO authored library under any license. If given the circumstances, it is unavoidable, create an *intermediate compiled layer* which then accesses the specific functions in that tainted library. We must then work earnestly to get rid of such taints ASAP.


##### Implication

*Any* software artifact we assemble, develop, release or distribute will always be constructed out of the above artifacts and will always be released under a intent-based dual-license AGPLv3+ (for community) and CC-BY-NC-ND 4.0 international (for commercial).

We always target the interface and provide an implementation for that interface. The interface should be available [royalty free](https://en.wikipedia.org/wiki/Royalty-free) without [encumberance](https://en.wikipedia.org/wiki/Encumbrance), [export controls](https://www.trade.gov/us-export-controls) or [embargo](https://en.wikipedia.org/wiki/Economic_sanctions).

We *re-implement* any library which provides the slightest of blocker to the four freedoms or is vendor controlled by way of trade mark, service mark.

Should we observe that there are two compelling interfaces that create value for the customer, eg. ([ASN.1](https://en.wikipedia.org/wiki/ASN.1) and gRPC) or ([Cap_n_Proto](https://capnproto.org) and gRPC), then we implement both the interfaces along with a custom gateway implementation.


##### Observation

When it comes to Testing or QA, in all scenarios we implement our own vendor-free unit testing, mocking, data set generation libraries and frameworks.

The free software community requires build systems and dependency management tools for C++20 and we are going to explore and engage with projects which are pushing the envelope.

We believe our approach provides immense opportunity for innovation in what we do and the free software community that we are part of.


##### Legend

* 'C-orig' implies country of origin restriction
* 'no bar' implies that download is available from non_US sites.
* 'LFS' refers to [Linux from Scratch](http://www.linuxfromscratch.org) project.
* 'taint' refers to a trace of a bad or undesirable substance or quality.

