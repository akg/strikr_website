
![Strikr Tux Query](../img/tux_query.png)

##### Frequently Asked Questions (FAQ) | Rarely Asked Questions (RAQ)

Here is a growing list of questions that we have been asked during the sessions.

 - What is the single most important thing at Strikr Free Software project ?
   <br />
   Realizing the vision, Remove inequality by Production of Knowledge is the single most important thing.

 - What defines the moral compass of the community ?
   <br />
   Our traditions of 'Justice' and 'Meritocracy'.

 - What is Copyleft ?
   <br />
   The idea of copyleft is that *Modified* and *Extended* versions must be under the **same** license !

 - Who is the BDFL of Strikr Free Software project ?
   <br />
   There is no BDFL here.

 - Where is the board of directors ?
   <br />
   There is no board of directors.

 - What role does the steering commitee play ?
   <br />
   There is no such commitee.

 - How is the community managed and supported ?
   <br />
   with Facilitator-Contributor duality.

 - Can a corporate become member of Strikr ?
   <br />
   only individuals can.

 - Do you accept donations ?
   <br />
   yes, donations are very welcome and accepted from individuals.

 - Will you succeed in your goal ?
   <br />
   Inshallah.


<hr/>

art credit: located in public domain. please help us locate the author. if you know, please write to legal@strikr.io
