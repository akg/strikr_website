
![Strikr GNU vision](../img/gnu.png)

### Vision

to *remove* inequality by production of knowledge.



![Strikr mission](../img/gnu_linux.png)

### Mission

to *accelerate* the production of free infrastructure software and create a *compelling* alternative to MAFANGO geo-locked proprietary infrastructure software.



![Strikr milestone](../img/milestone.png)

### Milestone

* milestone 01 - we seek to *achieve* our target in 48 months (2020-04 - 2024-04).



<hr/>

art credit: GNU logo, GNU project. GNU Linux art by [Rui Damas](https://www.gnu.org/graphics/gnu-slash-linux.html)