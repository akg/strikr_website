
![Strikr Legal](../img/tux_legal.png)

Ensuring software freedom requires an understanding of the law, market dynamics and a vision for the future. Hence it is very important to make a informed choice about which software license should be used.

All artifacts are released under AGPLv3.


![AGPLv3](../img/agplv3.png)

##### AGPLv3

The [GNU Affero General Public License](./license/agplv3.md) is a free, copyleft license for software and other kinds of works, specifically designed to ensure cooperation with the community in the case of *network server software*.

[Why the Affero GPL](https://www.gnu.org/licenses/why-affero-gpl.en.html) makes for a insightful read and why you should always used AGPLv3.


![AGPLv3](../img/gfdlv3.png)

##### GFDLv3

The [GNU Free Documentation License](./license/gfdlv3.md) is a form of copyleft intended for use on a manual, textbook or other document to assure everyone the effective freedom to copy and redistribute it, with or without modifications, either commercially or non-commercially.

We are authoring technical documentation, books and tutorials. The two links below are a must read for potential contributors and newbies.

* [Why publishers should use the GNU FDL](https://www.gnu.org/licenses/why-gfdl.html)
* [Tips on Using the GNU FDL](https://www.gnu.org/licenses/fdl-howto.html)


![CC-BY-NC-ND](../img/ccbyncnd.png)

##### CC-BY-NC-ND

[Creative Commons by Attribution Non-Commercial No-Derivative 4.0](./license/ccbyncnd.md) International license.

According to the Wikipedia, the [Creative Commons](https://creativecommons.org) licenses all grant "baseline rights", such as the right to distribute the copyrighted work worldwide for non-commercial purposes and without modification.

Licensees must know the four points about CC-BY-NC-ND:

* BY - attribution - require *attribution* to the creator.
* SA - sublicense with compatible but more restrictive licence clause.
* NC - only for non-commercial purposes (work, derivative work and any remixes)
* ND - *verbatim* copies may be displayed, copied and distributed; *modified* copies can be used but not shared. 


![CopyLeft](../img/copyleft.png)


##### Why we make these choices ?

In a real world, copyright law affords protection to a creator's work, software source code in this case. However, the work has little social or economic value in isolation. That's where choice of software license becomes very important.

[Copyleft](https://en.wikipedia.org/wiki/Copyleft) is the practice of granting the right to freely distribute and modify intellectual property with the requirement that the *same rights be preserved in derivative works* created from that property.

A strong copyleft license is absolutely critical to ensuring the availability of the source code (original work) **and** availability of all the changes made (derivative work) independent of the field of endeavour.

We see it as *honor carrying code* and everyone in the value chain, be it the creator of the original work, the conveyor, the remixer or the end user are not only aware but also can detect where the trust in that chain breaks. Without honor, you cannot count on someone to do the right thing unless you are watching them all the time, and that is impossible to do.

Another way to look at it is that, network of individuals having organized themselves as organizations whether incorporated or unincorporated all co-operate together on a level-playing field enforced by the copyleft (cf. Linux development process) while they *compete* over services and other aspects in a free market.

We see substantial value in increasing the body of free software as it is the bullwark against *patents* by helping create *prior art*.

However, we are also aware of vendor tactics such as open-core, lobby 'fronts', corporate funded foundations, developer advocate marketing etc. When seen from the view-point of software freedom, these coercive games are a pathology of short-sighted rent-seeking corporations.

With *courage* and *perseverence* we seek to create *compelling* alternative to the solutions which today potential users and network of indviduals seek earnestly. Together with enlightened individuals across the globe and the motivation to make the world a better benign and equitable place for today's generation and the generations to come.

Innovation happens the creator of the original work can operate in an environment which protects, sustains h(er/is) creation. The four-freedoms are the foundation of a strong copy-left software movement.

* 0: The freedom to run the program as you wish, for any purpose (freedom 0)
* 1: The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
* 2: The freedom to redistribute copies so you can help others (freedom 2).
* 3: The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

To know more, please read [What is Free Software](https://www.gnu.org/philosophy/free-sw.html)

Free software succeeds when the community gets together and engages vigorously in the production of knowledge.


We truly believe that we can be *altruistic* and *successful* !


<hr />
image credit: free software foundation, creativecommons website.