#-----------------------------------------------------------
# Strikr Free Software Project
# https://strikr.io/
#-----------------------------------------------------------


site_name: Strikr Free Software Project

site_url:  https://strikr.io/

site_author: strikr

site_description: Software Freedom Foundation, Free Software for the Enterprise, Cloud Infrastructure, Container Infrastructure, Function as a Service, Serverless, Virtualization, Containerization, ContainerNative, Networking, Storage, Server, Scalability, ISO C++, Cplusplus, modern C++20, modern C++23, C++ modules, Linux kernel, GNU Compiler Collection, GCC, GLIBC, Curses, Modern Software Engineering, Git, DevOps, DevSecOps, Software Defined Everything, Free Software, Ansatz, Continental, Kick, Punch, InterModal, Aykiz, Turgut, meXem, OBA, Altan, Engin, Demir, Focus, Zone, PostgreSQL, ProgreSQL, Strikr, StrikrIO, Strikr Free Software Project. GFDLv3. AGPLv3, Affero GNU Public License v3 or above, GNU Free Documentation License v3 or above. Saturday Community Meet, Daily Community Hours, With All Due Respect (WADR) event.

copyright: Copyright &copy; 2019,2020,2021 Strikr Free Software Project. Software Freedom Foundation. All documentation is released under GNU Free Documentation License <a href='https://www.gnu.org/licenses/fdl-1.3.html'>GFDLv3+</a>. All source code is released under GNU Affero General Public License <a href='https://www.gnu.org/licenses/agpl-3.0.html'>AGPLv3</a>.


strict: true

docs_dir: docs

site_dir: /opt/www/output/

use_directory_urls: true



theme:
      name:          bootstrap4
      custom_dir:    theme_override
      nav_style:     dark
      next_prev:     false
      next_previous: false
      suppress_nextprev: true
      include_sidebar:   false
      font:          false
      lang:          en
      logo:          'img/strikr.png'
      favicon:       'img/favicon.ico'
      highlightjs:   true
      hljs_languages:
            - cc
            - cpp
            - perl
            - pm
            - pl
            - docker
            - dockerfile
            - lisp
            - make
            - makefile
            - txt
            - text
            - plaintext
            - r
            - bash
            - shell
            - console
            - json
            - yaml


markdown_extensions:
      - pymdownx.superfences:
            custom_fences: [
                  { name: sequence, class: uml-sequence-diagram }
            ]


extra_css:
      - css/strikr.css


extra_javascript:
      - js/strikr.js
      - js/raphael-min.js
      - js/webfont-min.js
      - js/snap.svg-min.js
      - js/underscore-min.js
      - js/flowchart-min.js
      - js/sequence-diagram-min.js


extra:
      version: 0.0.1



plugins:
      - bootstrap4-blockquotes
      - bootstrap4-tables
      - search:
            lang: en
            prebuild_index: true
            indexing: full
#      - i18n:
#            default_language: en
#            languages:
#                  tr: Türkçe
#                  ar: اَلْعَرَبِيَّةُ
#                  zh: 中文
#                  de: Deutsch
#                  es: Español
#      - minify:
#            minify_html: true



#-------------------------------------------------------------------------------

nav:

#      - Home:     index.md

      - Governance:
            - Our Goals:                  governance/goals.md
            - What is:                    governance/whatis.md
            - Values:                     governance/values.md
            - Leadership:                 governance/leadership.md
            - Code of Conduct:            governance/conduct.md
            - FAQ / RAQ:                  governance/faqraq.md
            - Choice of License:          governance/choice.md
            - GNU Affero GPLv3+:          governance/license/agplv3.md
            - GNU FDLv3+:                 governance/license/gfdlv3.md
            - CC-BY-NC-ND 4.0:            governance/license/ccbyncnd.md
            - Strikr Logo:                governance/logo.md
            - Strikr Domain name:         governance/domain.md
            - Strikr IANA PEN:            governance/iana.md
      - Community:
            - Daily Hours:                community/daily.md
            - Saturday Meet:              community/meet.md
            - Hackathon:                  community/hackathon.md
            - WADR:                       community/wadr.md
            - Event Timings:              community/timings.md
            - Event Schedule:             community/event/index.md
            - IRC:                        community/irc.md
            - Jitsi:                      community/jitsi.md
            - Mailing List:               community/list.md
            - Social Media:               community/social.md
            - Acronyms (Lingo):           community/acronym.md
      - Engineering:
            - Architectural Principles:   engineering/architecture.md
            - Baseline:                   engineering/baseline.md
            - Emacs:                      engineering/emacs.md
            - Git Branches:               engineering/git.md
            - GPG Keys:                   engineering/gnupg.md
            - GNU C++ Compiler:           engineering/gcc.md
            - C++20 Overview:             engineering/cplusplus.md
            - C++20 Demangling:           engineering/demangling.md
            - C++20 Standard Library:     engineering/standardlibrary.md
            - C++20 Modules:              engineering/modules.md
      - Projects:
            - Team:
                  - Communication:        project/team/communication.md
                  - Meetings:             project/team/meetings.md
                  - Members:              project/team/members.md
            - C++20 Book:                 project/cplusplusbook/index.md
            - Website:                    project/website/index.md
            - InterModal:                 project/intermodal/index.md
      - Download:
            - GCC C++ Compiler:           download/gcc.md
#-------------------------------------------------------------------------------
